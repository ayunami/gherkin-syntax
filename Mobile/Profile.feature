@update_profile
Feature: Update Profile
    Background: user is already on the My Account page and user click Edit button
        Given user is already on the My Account page
        When user click Edit button

    @positive_test
    Scenario: TC.Upd.Prof.001.001 - User can update profile
        When user select image from Gallery
        Then user can see a pop-up message informing that Profil berhasil diperbaharui
    
    @negative_test
    Scenario Outline: User can not update profile
        When user click <field> field
        And user update profile but <conditions>
        And user click Simpan button
        Then user can see a pop-up message informing that <result>

    Examples:
        |        case_id       |    field   |        conditions            |    result     |
        | TC.Upd.Prof.001.002  | Nama       | leave Nama field empty       |  Wajib diisi  |
        | TC.Upd.Prof.001.003  | Nomor Hp   | leave Nomor Hp field empty   |  Wajib diisi  |
        | TC.Upd.Prof.001.004  | Alamat     | input spaces in Alamat field |  Wajib diisi  |