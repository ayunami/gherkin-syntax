@logout
Feature: Logout

    Scenario: TC.Log.002.001 - User want to logout
        Given user is already on the My Account page
        When user click Keluar button
        Then user successfully log out