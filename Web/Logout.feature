@logout
Feature: Logout

    @positive_test
    Scenario: TC.Log.002.001 - User want to logout
        Given user is already on Homepage
        When user click Profile button in header
        And user click Keluar
        Then user successfully logout